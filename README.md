# Logger

Track anything and get cool reports


- Commits pushed
- Tests run/passed/failed
- Trello cards created

## Graphs

use http://www.chartjs.org/

## IFTTT

IFTTT has a Maker thing which you can create custom triggers and actions.

Actions can create a POST request to a url.
