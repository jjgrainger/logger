<?php


class Bitbucket extends Service
{
    // set up the service
    function setup()
    {

    }

    function route()
    {
        // when a post request is made this URI
        return '/webhooks/bitbucket';
    }

    // process the POST
    function process()
    {
        // take the date we want from the request
        // and assign it to an event

        $activity = new Activity([

            'service' => 'Bitbucket',
            'event' => 'Repository',
            'action' => 'Created',

            // if applies to a specific user
            'user' => $user->id, // have to find user in db

            // auto generated or can be specified
            'timestamp' => timestamp(),

            // add additonal meta for filters/measures
            'meta' => [
                'repository' => 'venncreative/deliveroo',
                'repository_url' => '',
                '_raw' => '' // raw json, incase ever missed something
            ]
        ]);
    }


}
